

### What is this repository for? ###

Updated version of the original ZeldaGrass plugin for Minecraft Bukkit/Spigot servers! All credit/rights go to the original author Errored!
A lightweight and easy plugin to use. 
* There is a 1/50 (2%) chance of getting "Rupees" (Emeralds) and "Hearts" (Health) when you cut grass with any sword. 
* Added "Blue Rupees" (Diamonds) for a 1/1000 chance.


### How do I get set up? ###

Toss it in your plugins folder! There are still no permissions. 
Nothing needs to be configured by default in the config file. 
However, if you're feeling brave, you can now modify the Item Type dropped, the drop rate and the message!

### Who do I talk to? ###

Suggest something in the comments below,PM or Email me at doublehelix457.dev@gmail.com! 
If any problems come up, please submit an issue on the issues page, or on the bukkit website. 
[https://dev.bukkit.org/projects/zeldagrassplus/issues]. 